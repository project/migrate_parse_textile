Migrate: Parse Textile
--------------------
Parses textile into HTML (if detected) in a field during a migration.


Related modules
--------------------------------------------------------------------------------
The following modules are very useful for dealing with migrations:

* Migrate Upgrade
  https://www.drupal.org/project/migrate_upgrade
  Lets you run the Drupal 6 or 7 upgrade process to D8 or 9 via Drush.

* Migrate Tools
  https://www.drupal.org/project/migrate_tools
  Provides Drush integration for running Migrate commands.


Credits / contact
--------------------------------------------------------------------------------
Currently maintained by John Ouellet [1].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://www.drupal.org/project/issues/migrate_parse_textile


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/u/labboy0276
