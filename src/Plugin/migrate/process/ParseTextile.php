<?php

namespace Drupal\migrate_parse_textile\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateException;
use Netcarver\Textile\Parser;

/**
 * Parses textile into HTML if detected in a field during migration.
 *
 * @MigrateProcessPlugin(
 *   id = "parse_textile"
 * )
 *
 * To do check the lookup use the following:
 *
 * @code
 * field_text:
 *   plugin: parse_textile
 *   source: value
 * @endcode
 */
class ParseTextile extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    // Build out the regex string.
    $regex = "/";

    // Headers.
    $regex .= '(:?^|\s)(h[1-6])(\.|\<\.|\>\.|\=\.|\<\>\.|\(\.|\(\(\.|\(\(\(\.)\s';

    // Paragraphs.
    $regex .= '|(:?^|\s)(p)(\.|\<\.|\>\.|\=\.|\<\>\.|\()';

    // Pre-formatted text.
    $regex .= '|(:?^|\s)pre\.';

    // Block code.
    $regex .= '|(:?^|\s)bc\.';

    // Block quotations.
    $regex .= '|(:?^|\s)bq\.';

    // Textile comments.
    $regex .= '|###\.';

    // No formatting (override Textile).
    $regex .= '|(:?^|\s)notextile\.';

    // Bulleted (unordered) lists.
    $regex .= '|(\*|\*\*)';

    // Numbered (ordered) lists.
    $regex .= '|(#|##)\s';

    // Block quotations.
    $regex .= '|(:?^|\s)bq\.';

    // Footnotes.
    $regex .= '|(:?^|\s)(fn[1-6])\.';

    // Endnotes (auto-numbered notes).
    $regex .= '|(:?^|\s)note#';

    // Links.
    $regex .= '|":';

    // Images.
    $regex .= '|!\/';

    // Italic text.
    $regex .= '|__';

    // Citations.
    $regex .= '|\?\?';

    // Closing.
    $regex .= '/m';

    // Check for matches and flatten to remove false positive.
    preg_match_all($regex, $value, $out);
    $check = array_filter($out);

    // Parse the text if matches.
    if (!empty($check)) {
      $value = (new Parser())->parse($value);
    }

    return $value;
  }

}
